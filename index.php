<!doctype html>
<html lang="en">
<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-51910479-21"></script>
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-51910479-21');
  </script>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <!-- Roboto font goolefonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

  <!-- Fontawesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

  <!-- Cactus CSS -->
  <link rel="stylesheet" href="assets/css/app.css"/>

  <title>Prosegur Alarmas</title>

  <?php
  //GET LANDING
  $background = 'bg-01';
  $id_landing = 1;
  $title = 0; //alarmas
  if (isset($_GET['landing']) && $_GET['landing'] != ''){
    switch ($_GET['landing']) {
      case '1':
      $background = 'bg-01';
      break;
      case '2':
      $background = 'bg-02';
      $id_landing = 2;
      break;
      case '3':
      $background = 'bg-03';
      $id_landing = 3;
      break;
      case '4':
      $background = 'bg-04';
      $id_landing = 4;
      $title = 1;
      break;
      case '5':
      $background = 'bg-05';
      $id_landing = 5;
      $title = 1;
      break;
      case '6':
      $background = 'bg-06';
      $id_landing = 6;
      $title = 1;
      break;
    }
  }
  $landing_names = array(
    'landing-name',
    'landing-name',
    'landing-name',
    'landing-name',
    'landing-name',
    'landing-name'
  );

  /**
   * GET PLATFORM
   * if exist platform and is equal to "email" -> email if not -> SEM
   * separates which leads come from email to the rest.
   */
  $platform = (isset($_GET['platform']) && $_GET['platform'] == 'email') ? 'email' : 'SEM';

  if (isset($_GET['source']) && $_GET['source'] != '' && !isset($_GET['platform'])) {
    $platform = (isset($_GET['source']) && $_GET['source'] == 'email') ? 'email' : 'SEM';
  }


/**
 * GET PROVIDER
 * Si nos llega el parámetro provider desde la url y no está vacío se le asignará
 * un código para cada uno de los distintos soportes
 *
 * 0 -> no hay provider / soporte
 * 1 -> Adsalsa
 * 2 -> Arkeero
 * 3 -> Afilead
 * 4 -> Triboo
 * 5 -> Leads Global
 */
  $soporte = array(
    "Adsalsa", // 1
    "Arkeero", 
    "Afilead",
    "Triboo",
    "Leads-Global", // 5
    "C-DIRECT",
    "Vip-response",
    "Prosegur_Content",
    "Vértigo",
    "Admindsdigital", // 10
    "smart4ads",
    "datawork",
    "feebo"
  );
  $provider = (isset($_GET['provider']) && $_GET['provider'] != '') ? $_GET['provider'] : '0';


  /**
   * GET DATE
   * Date that is fullfilled for each one of providers when the email was sent
   */
  $date = (isset($_GET['date']) && $_GET['date'] != '') ? $_GET['date'] : '';


  /**
   * GET IDOrigen
   * This param is fullfilled for each one of providers
   */
  $id_origen = (isset($_GET['IDOrigen']) && $_GET['IDOrigen'] != '') ? $_GET['IDOrigen'] : '';

  ?>

  <!-- cookies alert -->
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
  <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
  <script>
  window.addEventListener("load", function(){
  window.cookieconsent.initialise({
    "palette": {
      "popup": {
        "background": "#eeeeee"
      },
      "button": {
        "background": "#fed300"
      }
    },
    "showLink": false,
    "content": {
      "message": "PROSEGUR utiliza Cookies propias para elaborar estadísticas y mostrar productos relacionados con sus preferencias mediante el análisis de sus hábitos de navegación. Si continua navegando o pulsa sobre el botón aceptar consideramos que acepta su uso. <a href='#' class='d-inline-block' data-toggle='modal' data-target='#p-cookies'>Saber más.</a>",
      "dismiss": "Aceptar"
    }
  })});
  </script>

</head>
<body>



  <input type="hidden" id="id_landing" name="" value="<?php echo $id_landing; ?>">
  <input type="hidden" id="platform" name="" value="<?php echo $platform; ?>">
  <input type="hidden" id="provider" name="" value="<?php echo $provider; ?>">
  <?php if ($provider > 0 && $provider < 14): ?>
    <input type="hidden" id="soporte" name="" value="<?php echo $soporte[$provider - 1]; ?>">
  <?php endif; ?>
  <?php if ($date != ''): ?>
    <input type="hidden" id="date" name="" value="<?php echo $date; ?>">
  <?php endif; ?>
  <?php if ($id_origen != ''): ?>
    <input type="hidden" id="id_origen" name="" value="<?php echo $id_origen; ?>">
  <?php endif; ?>


  <div class="modal fade" id="p-cookies" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Información sobre las cookies</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p><b>Política de Cookies</b></p>
          <p>Con el fin de facilitar su navegación por este Sitio Web, Prosegur Alarmas España, S.L. (en adelante, el “Prosegur”) con domicilio social en Calle Pajaritos, 24, 28007, Madrid (España), e inscrita en el Registro Mercantil de Madrid al Tomo 33220, Folio 200, Hoja M-597850, Inscripción 1ª y con CIF B87222006, le comunica que utiliza Cookies u otros archivos de funcionalidad similar (en adelante, las “Cookies”).</p>
          <p>En todo caso, le informamos de que Prosegur es el responsable de las Cookies y del tratamiento de los datos obtenidos a través de las Cookies propias y de terceros decidiendo sobre la finalidad, contenido y uso del tratamiento de la información recabada.</p>
          <p><b>¿Qué es una Cookie?</b></p>
          <p>Las cookies son pequeños ficheros de datos que se instalan en el ordenador o dispositivo móvil del usuario y que permiten que sea el propio usuario el que almacene o recupere la información que genera su actividad en la red, a través de su ordenador o de su dispositivo móvil. De esta manera se mejora y personaliza la experiencia del usuario con la Web y los servicios que esta ofrece.</p>
          <p><b>Uso de Cookies por parte del prestador</b></p>
          <p>Durante el acceso y navegación en la Web, Prosegur puede almacenar o registrar las direcciones IP, preferencias de usuario, el tipo de dispositivo utilizado, identificar al usuario, o almacenar y recuperar información de hábitos de navegación y cuya finalidad es estadística en relación con el número de visitas o tráfico de la Web, además de para ofrecer una experiencia más personalizada en su navegación, o recordar sus datos para iniciar sesión incluso en diferentes dispositivos.</p>
          <p>Este sitio Web de Prosegur puede utilizar las siguientes cookies, ya sean propias o de terceros autorizados:</p>
          <ul>
            <li><b>Cookies técnicas:</b> Son necesarias para el correcto funcionamiento de la Web. Estas cookies son memorizadas y únicamente tienen validez temporal. Estas cookies no graban de forma permanente ninguna información en el disco duro de su ordenador, y son básicas para controlar el tráfico y la comunicación de datos, iniciar sesión o realizar el proceso de compra, o compartir contenidos a través de redes sociales, entre otros.
            <br>
            Estas cookies basan su utilidad en el seguimiento temporal de la navegación por Internet. El usuario tiene la posibilidad de eliminar este tipo de cookies antes de iniciar la navegación por otras páginas del sitio web.<br>
            Dentro de estas Cookies podemos encontrar, entre otras cookies de entrada de usuario, de autentificación, de seguridad de usuario, de sesión de reproductor multimedia, o las cookies de complemento (plug in) para intercambiar contenidos sociales.
            </li>
            <li><b>Cookies de personalización:</b> Son aquéllas que permiten al usuario acceder al servicio con algunas características de carácter general predefinidas en función de una serie de criterios en el terminal del usuario como por ejemplo serian el idioma, el tipo de navegador a través del cual accede al servicio, la configuración regional desde donde accede al servicio, o recordar búsquedas anteriores de tus destinos, etc.</li>
            <li><b>Cookies de análisis o estadísticas:</b> Las cookies estadísticas permiten conocer el nivel de recurrencia de nuestros visitantes y los contenidos que resultan más interesantes. De esta manera podemos concentrar nuestros esfuerzos en mejorar las áreas más visitadas y hacer que el Usuario encuentre más fácilmente lo que busca.<br>
            Prosegur puede utilizar la información de su visita para realizar evaluaciones y cálculos estadísticos sobre datos anónimos, así como para garantizar la continuidad del servicio o para realizar mejoras en sus sitios Web. Dicha información no será utilizada para ninguna otra finalidad. Prosegur puede utilizar cookies de análisis de terceros.</li>
            <li><b>Cookies publicitarias:</b> permiten la gestión, de la forma más eficaz posible, de los espacios publicitarios incluidos en nuestra página web, en base a criterios como el contenido editado o la frecuencia en la que se muestran los anuncios.</li>
            <li><b>Cookies de publicidad comportamental:</b> Permiten que los anuncios que veas en nuestra Web o en otras páginas web sean más personalizados y acordes con tus gustos, evitando recomendaciones no afines a tus intereses o preferencias, y si ofreciéndote propuestas comerciales dirigidas, desarrollando un perfil específico basándonos, por ejemplo, en tus búsquedas u otros hábitos de navegación. Prosegur puede utilizar cookies publicitarias de terceros.</li>
            <li><b>Cookies de geolocalización:</b> Permiten conocer la localización del Usuario, previo consentimiento, para permitir la prestación de determinados servicios o personalizar la experiencia del usuario.</li>
            <li><b>Web beacons:</b> Prosegur puede utilizar por sí mismo o a través de terceros web beacons, es decir, una pequeña imagen gráfica que permite identificar si la visualización de un anuncio determinado ha permitido la contratación de un servicio o la adquisición de un producto, de manera que podamos monitorizar los resultados de las webs de afiliados, además de para análisis estadísticos.</li>
            <li><b>Pixel tags:</b> Esta tecnología permite que tanto en la Web como en correos electrónicos se pueda hacer un seguimiento del comportamiento del Usuario.</li>
            <li><b>Cookies de complemento de redes sociales para el seguimiento:</b> Las cookies de complemento “plug in” para intercambiar contenidos sociales pueden ser utilizadas por terceros para realizar el seguimiento de personas, tanto miembros como no miembros de una red social, para incluir publicidad comportamental o para el análisis y la investigación de mercados.</li>
            <li><b>Cookies de Tests AB:</b> Nuestro Sitio utiliza una solución de A/B testing que utiliza cookies para mejorar la experiencia del usuario. Al utilizar nuestro Sitio, acepta dicho uso y su finalidad. Para retirar su consentimiento u obtener más información por favor visite la <a href="https://www.abtasty.com/es/aviso-legal/" target="_blank">Página de Privacidad de AB Tasty</a>.
          </ul>
          <p>Prosegur también utiliza Local storage o almacenamiento local que permite recopilar y almacenar información personal de forma local en el dispositivo del usuario mediante tecnología HTML5, de manera que incluso después de haber cerrado el navegador, se pueden recuperar los datos. Aunque el funcionamiento es similar a una cookie, el Local storage tiene algunas diferencias ya que puede ocupar más espacio, la información almacenada no es enviada al servidor en cada petición y no tiene caducidad, por lo que habrá que eliminarlo expresamente.</p>
          <p>A continuación, se incluye la siguiente tabla, con identificación de las cookies utilizadas en el Sitio Web de Prosegur, así como si son propias o de terceros (e identificación del tercero contratado o cuyos servicios se han decidido utilizar) y su finalidad:</p>
          <p><b>Cookies utilizadas en este sitio Web.</b></p>
          <table class="mb-3">
            <tr>
              <td class="border">Proveedor</td>
              <td class="border w-50">Google</td>
            </tr>
            <tr>
              <td class="border">Finalidad</td>
              <td class="border w-50">Habilitan la función de registro de la procedencia del usuario. Los datos registrados serán, entre otros, si el usuario llega a nuestro sitio Web por tráfico directo, desde otra web, desde una campaña publicitaria o desde un buscador (indicando la palabra clave utilizada y la fuente.</td>
            </tr>
            <tr>
              <td class="border">Dónde se encuentran</td>
              <td class="border w-50">.google.com</td>
            </tr>
            <tr>
              <td class="border">Permanencia</td>
              <td class="border w-50">Permanente</td>
            </tr>
            <tr>
              <td class="border">Nombre de las cookies</td>
              <td class="border w-50">CONSENT</td>
            </tr>
          </table>
          <table class="mb-3">
            <tr>
              <td class="border">Proveedor</td>
              <td class="border w-50">Google Analytics</td>
            </tr>
            <tr>
              <td class="border">Finalidad</td>
              <td class="border w-50">.google.es, .ofertas-prosegur.es</td>
            </tr>
            <tr>
              <td class="border">Dónde se encuentran</td>
              <td class="border w-50">Permite a Google Analytics recopilar información sobre la procedencia del usuario, su interacción con la web y limitar el porcentaje de solicitudes.</td>
            </tr>
            <tr>
              <td class="border">Permanencia</td>
              <td class="border w-50">Permanente, salvo _gid, que se guarda durante 24 horas</td>
            </tr>
            <tr>
              <td class="border">Nombre de las cookies</td>
              <td class="border w-50">1P_JAR, __gat_gtag_[UID], _gid</td>
            </tr>
          </table>
          <table class="mb-3">
            <tr>
              <td class="border">Proveedor</td>
              <td class="border w-50">Google Analytics</td>
            </tr>
            <tr>
              <td class="border">Finalidad</td>
              <td class="border w-50">.google.es, .ofertas-prosegur.es</td>
            </tr>
            <tr>
              <td class="border">Dónde se encuentran</td>
              <td class="border w-50">Se usa para distinguir usuarios y sesiones. La cookie se crea cuando se ejecuta la biblioteca JavaScript y no hay ninguna cookie __utma. La cookie se actualiza cada vez que se envían datos a Google Analytics.</td>
            </tr>
            <tr>
              <td class="border">Permanencia</td>
              <td class="border w-50">Dos años a partir de la configuración o actualización</td>
            </tr>
            <tr>
              <td class="border">Nombre de las cookies</td>
              <td class="border w-50">__utma</td>
            </tr>
          </table>
          <table class="mb-3">
            <tr>
              <td class="border">Proveedor</td>
              <td class="border w-50">Google Analytics</td>
            </tr>
            <tr>
              <td class="border">Finalidad</td>
              <td class="border w-50">.google.es, .ofertas-prosegur.es</td>
            </tr>
            <tr>
              <td class="border">Dónde se encuentran</td>
              <td class="border w-50">Almacena la fuente de tráfico o la campaña que explica cómo ha llegado el usuario al sitio. La cookie se crea cuando se ejecuta la biblioteca JavaScript y se actualiza cada vez que se envían datos a Google Analytics.</td>
            </tr>
            <tr>
              <td class="border">Permanencia</td>
              <td class="border w-50">Seis meses a partir de la configuración o actualización </td>
            </tr>
            <tr>
              <td class="border">Nombre de las cookies</td>
              <td class="border w-50">__utmz</td>
            </tr>
          </table>
          <table class="mb-3">
            <tr>
              <td class="border">Proveedor</td>
              <td class="border w-50">CloudFlare</td>
            </tr>
            <tr>
              <td class="border">Finalidad</td>
              <td class="border w-50">La cookie '__cfduid' es establecida por el servicio de CloudFlare para identificar tráfico web de confianza. No corresponde a ningún id de usuario en la aplicación web, ni guarda ningún dato personal identificable.</td>
            </tr>
            <tr>
              <td class="border">Dónde se encuentran</td>
              <td class="border w-50">.cloudflare.com </td>
            </tr>
            <tr>
              <td class="border">Permanencia</td>
              <td class="border w-50">5 años</td>
            </tr>
            <tr>
              <td class="border">Nombre de las cookies</td>
              <td class="border w-50">__cfduid</td>
            </tr>
          </table>
          <table class="mb-3">
            <tr>
              <td class="border">Proveedor</td>
              <td class="border w-50">Propia</td>
            </tr>
            <tr>
              <td class="border">Finalidad</td>
              <td class="border w-50">Recuerda la aceptación de la política de cookies. </td>
            </tr>
            <tr>
              <td class="border">Dónde se encuentran</td>
              <td class="border w-50">ofertas-prosegur.com</td>
            </tr>
            <tr>
              <td class="border">Permanencia</td>
              <td class="border w-50">1 año</td>
            </tr>
            <tr>
              <td class="border">Nombre de las cookies</td>
              <td class="border w-50">cookieconsent_status</td>
            </tr>
          </table>
          <p><b>Gestión de Cookies.</b></p>
          <p>Puede configurar su navegador para aceptar o rechazar por defecto todas las cookies o para recibir un aviso en pantalla de la recepción de cada cookie y decidir en ese momento su implantación o no en su disco duro. Para ello, le sugerimos acudir a la sección de ayuda de su navegador para saber cómo cambiar la configuración que actualmente emplea. También puede utilizar herramientas de bloqueo de cookies de rastreo del tipo “do not track”.</p>
          <p>Asimismo, puede revocar en cualquier momento el consentimiento prestado para la utilización de cookies por parte de Prosegur, configurando para ello su navegador. Por favor, consulte las instrucciones y manuales de su navegador para ampliar esta información, en los términos previstos a continuación:</p>
          <ul>
            <li>Si utiliza Microsoft Internet Explorer, en la opción de menú “Herramientas”, seleccionando “Opciones de Internet” y accediendo a “Privacidad”.</li>
            <li>Si utiliza Firefox, para Mac en la opción de menú “Preferencias”, seleccionando “Privacidad”, accediendo al apartado “Mostrar Cookies”, y para Windows en la opción de menú “Herramientas”, seleccionando “Opciones”, accediendo a “Privacidad” y luego a “Usar una configuración personalizada para el historial”.</li>
            <li>Si utiliza Safari, en la opción de menú “Preferencias”, seleccionando “Privacidad”.</li>
            <li>Si utiliza Google Chrome, en la parte superior derecha, seleccione “Más”, a continuación “Configuración”. En la parte inferior, haga clic en “Configuración avanzada”. Una vez en esta ventana, en "Privacidad y seguridad", haga clic en “Configuración de contenido” y, tras esto, en “Cookies”.</li>
          </ul>
          <p>Sin perjuicio de lo anterior, tenga en cuenta que la desactivación de cookies puede afectar al correcto funcionamiento de determinadas secciones de la página Web.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="p-privacidad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Política de Privacidad PROSEGUR ALARMAS</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p><b>Responsable del tratamiento:</b> Prosegur Alarmas España, S.L. (en adelante, “PROSEGUR”).</p>
          <p><b>Dirección:</b> Calle Pajaritos, 24, 28007, Madrid (España).</p>
          <p><b>Email:</b> dpo@prosegur.com</p>
          <p><b>Finalidad principal del tratamiento:</b> la finalidad será gestionar su solicitud, prestarle el servicio contratado, así como ofrecerle información de interés.</p>
          <p><b>Base legitimadora:</b> los datos se tratarán principalmente en base a la ejecución del contrato de servicios entre usted y PROSEGUR y la obtención del consentimiento.</p>
          <p><b>Potenciales destinatarios de los datos:</b><br/>
              • Empresas dentro del grupo empresarial al que pertenece Prosegur Alarmas España, S.L. (en adelante, el “Grupo PROSEGUR”), incluyendo fuera del Espacio Económico Europeo.</p>
          <p><b>Derechos de protección de datos:</b> usted puede revocar en cualquier momento el consentimiento otorgado para las actividades de tratamiento de datos personales por parte de PROSEGUR, así como ejercitar, si está interesado en ello, sus derechos de acceso, rectificación, supresión, oposición, limitación al tratamiento de datos, portabilidad de datos, así como a no ser objeto de decisiones automatizadas.</p>
          <p><b>Información Adicional</b></p>
          <p><b><em>1. INFORMACIÓN ACERCA DE PROSEGUR</em></b></p>
          <p>1.1. Responsable del tratamiento</p>
          <p>El presente documento regula la política de privacidad de la página web de PROSEGUR (en adelante el, “Sitio Web”).
          Prosegur Alarmas cuenta con una amplia gama de servicios de seguridad, esta división de PROSEGUR se dirige tanto a residenciales como a negocios. Para su información, a continuación, se recogen los datos identificativos de la sociedad:
          Denominación social: Prosegur Alarmas España, S.L. (en adelante, “PROSEGUR”).<br/>NIF: B87222006<br/>Dirección: Calle Pajaritos, 24, 28007, Madrid (España)<br/>Contacto Delegado de Protección de Datos: dpo@prosegur.com</p>
          <p>1.2. Aplicación de esta Política de Privacidad</p>
          <p>La presente política de privacidad regula el tratamiento de datos llevado a cabo por PROSEGUR en relación con los usuarios que accedan o se pongan en contacto con PROSEGUR a través del Sitio Web – ofertas-prosegur.es–. Lo anterior abarcaría el tratamiento de datos personales de clientes y/o potenciales clientes que accedan o transmitan información a través del Sitio Web. Por tanto, la presente política de privacidad refleja la información relativa al tratamiento con respecto a las diferentes categorías de interesados indicadas.</p>
          <p><b><em>2.FINALIDADES Y LEGITIMACIÓN</em></b></p>
          <p>Usted queda informado de que todos los datos que PROSEGUR le solicite o le pueda solicitar son necesarios para las finalidades descritas en la presente política de privacidad y el hecho de no facilitarlos supondría la imposibilidad de poder contactar con usted o gestionar la solicitud que realice a PROSEGUR. Asimismo, PROSEGUR se reserva la facultad de no responder o tramitar solicitudes que no incluyan los datos que le sean solicitados.</p>
          <p>Usted garantiza la veracidad de los datos de carácter personal proporcionados a PROSEGUR. PROSEGUR puede solicitarle periódicamente la revisión y actualización de los datos personales que sobre usted conserve.</p>
          <p>En el Sitio Web usted puede encontrar información que puede ser de utilidad de cara a informarse de los servicios ofrecidos por PROSEGUR. En este sentido, PROSEGUR únicamente le solicitará y tratará los datos necesarios para prestar el servicio contratado o poder iniciar y mantener una relación comercial y/o contractual con usted.</p>
          <p>A continuación, se describen las diferentes finalidades con las que se tratan sus datos personales y las bases que legitiman el tratamiento:</p>
          <p><b>Legitimación por la ejecución del contrato de servicios entre usted y PROSEGUR o aplicación de medidas precontractuales:</b></p>
          <ul>
            <li>Gestionar la relación comercial con usted, realizando visitas comerciales y atendiendo a las solicitudes de información de productos y servicios ofertados por PROSEGUR.</li>
            <li>Formalizar la relación contractual, lo que comprenderá el proceso de contratación y firma de los contratos.</li>
            <li>Gestionar la relación contractual con usted y asegurar la prestación de los servicios contratados, lo que incluye tanto la asistencia en la instalación y el servicio técnico, como la facturación, cobro y baja de los mismos.</li>
            <li>En caso de haber contratado dichos servicios, utilizar datos relativos a la localización, con objeto de prestar servicios de teleasistencia y localización de vehículos.</li>
          </ul>
          <p><b>Legitimación por consentimiento expreso:</b></p>
          <ul>
            <li>Envío de comunicaciones comerciales relacionadas con distintos productos y servicios por cualquier vía, incluida la electrónica, de PROSEGUR y su Grupo de empresas dedicadas a los sectores de servicios auxiliares y vigilancia</li>
            <li>Cesión de datos a sociedades del Grupo PROSEGUR y a terceras entidades con la finalidad de remitirle comunicaciones comerciales.</li>
            <li>Utilizar los datos biométricos asociados a su firma para formalizar la relación contractual entre usted y el Grupo PROSEGUR.</li>
            <li>En caso de haber contratado dichos servicios, utilizar datos de salud de los clientes para gestionar la prestación de servicios de teleasistencia, tales como “Prosegur Siempre Contigo”.  </li>
          </ul>
          <p><b>Legitimación por intereses legítimos de PROSEGUR:</b></p>
          <ul>
            <li>Envío de comunicaciones comerciales relacionadas con productos y servicios similares de PROSEGUR por cualquier vía. En este caso el interés legítimo de PROSEGUR se basa en mantenerte informado y actualizado sobre productos o servicios que puedan ser de interés.</li>
            <li>Consultar Ficheros públicos de solvencia económica para verificar si está al corriente de pago, si tiene impagados y si los tuvo en el pasado cuánto tardó en pagarlos, lo que simplificará y facilitará el procedimiento de análisis de riesgos a la hora de contratar. En este caso, el interés legítimo de PROSEGUR sería preservar la seguridad de sus operaciones y prevenir el fraude.</li>
            <li>Realizar proyectos de automatización de procesos de negocio para mejorar los productos y servicios ofrecidos por PROSEGUR. El interés legítimo en este caso sería mejorar la eficiencia y productividad de la actividad de PROSEGUR.</li>
            <li>Realizar perfilados con base en sus datos personales, para calcular la predisposición de adquisición de otros productos o contratación de otros servicios, la propensión de anulación de los servicios contratados y la asignación de una evaluación con la finalidad exclusiva de realizar acciones comerciales.</li>
          </ul>
          <p><b><em>3. TERCEROS A LOS QUE SE LES PUEDE TRANSMITIR SUS DATOS</em></b></p>
          <p>Los datos personales que usted proporcione a PROSEGUR podrán ser comunicados a las siguientes categorías de destinatarios:</p>
          <ul>
            <li>Terceras partes a las que PROSEGUR está obligada a transmitir información, como autoridades públicas, con la finalidad de dar cumplimiento con los requerimientos de dichas autoridades y la normativa aplicable, en su caso.</li>
            <li>Empresas que formen parte del Grupo PROSEGUR, con la finalidad de poder gestionar adecuadamente su relación contractual como consecuencia de la centralización de procesos administrativos e informáticos existente dentro del Grupo PROSEGUR.</li>
            <li>Le informamos que el Grupo PROSEGUR puede contar con sociedades fuera de la Unión Europea. En esos casos, la entidad exige que dichas sociedades cumplan con las medidas diseñadas para proteger los datos personales establecidas en un contrato vinculante, salvo en los casos en que la Comisión Europea haya determinado que el país donde se encuentra ubicado el destinatario proporciona un nivel adecuado de protección de datos personales. Usted podrá obtener una copia de las medidas exigidas por PROSEGUR poniéndose en contacto con el Delegado de Protección de Datos.</li>
          </ul>
          <p><b><em>4. CONSERVACIÓN DE DATOS</em></b></p>
          <p>Los criterios que PROSEGUR utiliza para fijar los plazos de conservación de sus datos han sido determinados de acuerdo con los requisitos establecidos en la legislación, reglamentos y directrices normativas aplicables, así como los requisitos operacionales de PROSEGUR relacionados con la correcta gestión de la relación con sus clientes y/o potenciales clientes.</p>
          <p>Sus datos no serán conservados por un plazo superior a los seis años desde la finalización de cualquier posible relación contractual entre usted y PROSEGUR, habiendo sido fijado el anterior plazo de conservación de los datos bloqueados de acuerdo con la normativa mercantil, y más concretamente en base a lo establecido en el Código de Comercio. No obstante lo anterior, en el caso de que existiera un procedimiento procesal en curso en relación con usted, sus datos podrán ser conservados durante el tiempo adicional necesario hasta que se obtenga una resolución judicial firme. Una vez terminado dicho periodo, sus datos serán eliminados.</p>
          <p><b><em>5. SUS DERECHOS DE PROTECCIÓN DE DATOS</em></b></p>
          <p>Usted podrá ponerse en contacto cuando así lo estime con PROSEGUR, pudiendo ejercitar sus derechos de acceso, rectificación, supresión, limitación, oposición, así como el derecho a la portabilidad de sus datos y a no ser objeto de decisiones automatizadas, mediante solicitud dirigida a Prosegur Alarmas España, S.L. en Calle Pajaritos, 24, 28007, Madrid (España), o a la siguiente dirección de correo electrónico: protecciondedatos@prosegur.com, adjuntando copia de su DNI o documentación acreditativa de su identidad.</p>
          <p>Asimismo, se le informa que usted podrá revocar los consentimientos otorgados cuando así lo desee, contactando con PROSEGUR, a la dirección o correo electrónico indicados anteriormente.</p>
          <p>Por último, en el caso que Usted desee más información acerca de sus derechos en materia de protección de datos o precise presentar una reclamación podrá dirigirse a la Agencia Española de Protección de Datos, con domicilio en Calle Jorge Juan, 6, 28001, Madrid, con el fin de salvaguardar sus derechos.</p>
          <p><b><em>6. SEGURIDAD DE LOS DATOS</em></b></p>
          <p>PROSEGUR cuenta con políticas apropiadas y medidas técnicas y organizativas para salvaguardar y proteger sus datos personales contra el acceso ilegal o no autorizado, pérdida o destrucción accidental, daños, uso y divulgación ilegal o no autorizada.</p>
          <p>También tomaremos todas las precauciones razonables para garantizar que nuestro personal y los empleados que tienen acceso a sus datos personales hayan recibido la capacitación adecuada.</p>
          <p>En todo caso, se informa al usuario que cualquier transmisión de datos a través de Internet no es completamente segura y, como tal, se realiza bajo su propio riesgo. Aunque haremos nuestro mejor esfuerzo para proteger sus datos personales, PROSEGUR no puede garantizar la seguridad de los datos personales transmitidos a través de la Plataforma.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="p-legal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Aviso Legal</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p><b><em>1.- Información general</em></b></p>
          <p><b>PROSEGUR ALARMAS ESPAÑA, S.A.</b>, con N.I.F. número B-87222006, y domicilio social en Madrid, en la calle Pajaritos 24 - 28007 Madrid, inscrita en el Registro Mercantil de Madrid al tomo 33220, folio 200 y hoja número M-597850 (en adelante, “PROSEGUR”). Puede contactar con la mercantil a través del correo electrónico protecciondedatos@prosegur.com. </p>
          <p><b><em>2.- Condiciones</em></b></p>
          <p><b>2.1. Aceptación</b></p>
          <p>El presente Aviso Legal regula el acceso al sitio web -ofertas-prosegur.es- propiedad de PROSEGUR (en adelante, el “Sitio Web”) y el uso de los contenidos y de los servicios puestos a disposición de los Usuarios que acceden al Sitio Web (en adelante, los “Usuarios”).</p>
          <p>La utilización del Sitio Web por los Usuarios implica la aceptación plena y sin reserva alguna de todas las condiciones incluidas en el Aviso Legal que esté vigente en cada acceso al Sitio Web. Dado que PROSEGUR se reserva el derecho a modificar, en cualquier momento y sin necesidad de previo aviso, el presente Aviso Legal, el usuario debe leerlo atentamente en cada una de las ocasiones en que se proponga utilizar cualquiera de los contenidos y servicios ofrecidos en el Sitio Web, ya que éste puede haber sufrido modificaciones.</p>
          <p>Algunos servicios del Sitio Web pueden estar sometidos a condiciones particulares que sustituyen, complementan o modifican el presente Aviso Legal y que deberán ser igualmente aceptadas por los Usuarios antes de iniciarse la prestación del servicio en cuestión.</p>
          <p>PROSEGUR podrá libremente suspender, bloquear, interrumpir o cancelar el Servicio en  los siguientes casos:</p>
          <p>1.- para garantizar la seguridad del servicio o el canal de acceso,</p>
          <p>2.- cuando el Sitio Web sea utilizado de forma fraudulenta o abusiva,</p>
          <p>3.- cuando sea necesario realizar operaciones de mantenimiento o reparación del servicio, y</p>
          <p>4.- cuando se produzca un incumplimiento por parte del Usuario.</p>
          <p>El acceso al Sitio Web es gratuito salvo en lo relativo al coste de la conexión a través de la red de telecomunicaciones suministrada por el proveedor de acceso que hayan contratado los Usuarios. El acceso al Sitio Web, y especialmente, la contratación de los productos y servicios que se ofrecen a través del mismo, están reservados a personas mayores de 18 años, no haciéndose PROSEGUR responsable de las consecuencias derivadas del incumplimiento de esta medida.</p>
          <p><b>2.2. Uso</b></p>
          <p>Los Usuarios se comprometen a utilizar los servicios y contenidos del Sitio Web de conformidad con lo establecido en la Ley y en el presente Aviso Legal, respondiendo frente a Prosegur o frente a terceros, de los daños y perjuicios que pudieran causar como consecuencia del incumplimiento de dichas obligaciones, y de las actuaciones llevadas a cabo que sean de carácter ilícito, negligente, fraudulento, o que contravengan el principio de buena fe, los usos generalmente aceptados o el orden público.</p>
          <p>Queda expresamente prohibido el uso del Sitio Web con fines ilícitos, lesivos de los bienes, los intereses o la reputación de PROSEGUR o de terceros, o que de cualquier otra forma dañen, sobrecarguen o inutilicen las redes, servidores y demás equipos informáticos (hardware) o productos y aplicaciones informáticas (software) de PROSEGUR o de terceros.</p>
          <p><b>2.3. Contenidos y propiedad intelectual e industrial</b></p>
          <p>Todos los contenidos del Sitio Web, entendiendo por estos a título meramente enunciativo, los textos, fotografías, gráficos, imágenes, iconos, tecnología, software, links y demás contenidos audiovisuales o sonoros, su diseño gráfico y códigos fuente, así como las marcas, nombres comerciales o signos distintivos, son propiedad de PROSEGUR o de terceros. PROSEGUR no cede, ni concede licencia o autorización alguna a los Usuarios sobre los derechos de propiedad intelectual e industrial correspondientes a dichos elementos.</p>
          <p>Corresponde a PROSEGUR el ejercicio exclusivo de los derechos de explotación de la propiedad intelectual e industrial mencionada, en cualquier forma y, en especial, los derechos de reproducción, distribución, comunicación pública y transformación.</p>
          <p>Con carácter enunciativo pero no limitativo, y de conformidad con la legislación aplicable, se prohíbe a los Usuarios la realización de las siguientes acciones:</p>
          <p>1.- Reproducir, copiar, distribuir, poner a disposición, comunicar públicamente, transformar o modificar los contenidos salvo en los casos autorizados en la ley o expresamente consentidos por PROSEGUR.</p>
          <p>2.- Extraer y/o reutilizar la totalidad o una parte sustancial de los contenidos integrantes del Sitio Web que PROSEGUR ponga a disposición de los Usuarios.</p>
          <p>3.- Duplicar, copiar, vender, revender o explotar los Servicios de PROSEGUR con fines comerciales, sin nuestro consentimiento previo.</p>
          <p>4.- Utilizar cualquier marca comercial, logotipo u otra información protegida por derechos de autor de PROSEGUR sin la correspondiente autorización previa.</p>
          <p><b>2.4. Responsabilidad</b></p>
          <p><em>(i) De la información y los contenidos</em></p>
          <p>PROSEGUR procura que los contenidos recogidos en su Sitio Web sean de la mayor calidad posible, pero no garantiza la exactitud, exhaustividad y actualidad de los mismos. El acceso al Sitio Web y la utilización que pueda hacerse de las informaciones y contenidos que aparecen, es responsabilidad exclusiva de los Usuarios. PROSEGUR no responderá de las consecuencias, daños o perjuicios que pudieran derivarse de dicho acceso o uso de la información o de los contenidos.</p>
          <p><em>(ii) De la calidad y disponibilidad del acceso</em></p>
          <p>PROSEGUR no garantiza la ausencia de virus, gusanos o cualquier otro elemento informático dañino. Será responsabilidad de los Usuarios la utilización de las herramientas, medios o sistemas adecuados para la detección y desinfección de los elementos informáticos dañinos. PROSEGUR no se responsabiliza de los daños producidos en los equipos informáticos de los Usuarios o de terceros durante o como consecuencia del acceso al Sitio Web.</p>
          <p>El acceso al Sitio Web requiere de servicios y suministros provistos por terceros, sin que sea responsabilidad de PROSEGUR controlar la calidad, disponibilidad, continuidad y funcionamiento de los mismos. PROSEGUR no se responsabiliza de los daños o perjuicios de cualquier tipo producidos a los Usuarios que sean consecuencia de fallos, interrupciones o suspensiones del servicio de conexión a las redes de telecomunicaciones durante el acceso al Sitio Web.</p>
          <p>PROSEGUR no garantiza la falta disponibilidad, mantenimiento y efectivo funcionamiento del Sitio Web y/o de sus servicios. No obstante, pondrá sus mejores esfuerzos en procurar que el Sitio Web esté accesible y en pleno funcionamiento todo el tiempo. </p>
          <p><em>(iii) De los enlaces</em></p>
          <p>El Sitio Web puede incluir dispositivos de enlace que permitan a los Usuarios acceder a otras páginas y portales de Internet (en adelante, los "Sitios Enlazados"). En estos supuestos, PROSEGUR actúa como prestador de servicios de intermediación de conformidad con el artículo 17 de la Ley 32/2002, de 12 de julio, de Servicios de la Sociedad de la Información y el Comercio Electrónico y sólo será responsable de los contenidos suministrados en los Sitios Enlazados en la medida en que tenga conocimiento efectivo y manifiesto de su ilicitud y no haya desactivado el enlace con la diligencia debida.</p>
          <p>En el supuesto de que el Usuario considere que existe un Sitio Enlazado con contenidos ilícitos o inadecuados podrá comunicárselo a PROSEGUR para que lleve a cabo una investigación sobre la legalidad de los contenidos. No obstante, dicha comunicación no vincula a PROSEGUR a retirar el correspondiente enlace.</p>
          <p>Asimismo, se informa a los Usuarios que la existencia de enlaces en el Sitio Web no implica la existencia de acuerdos con los responsables o titulares de tales Sitios Enlazados, ni tampoco la recomendación, promoción o identificación de PROSEGUR con las manifestaciones, contenidos o servicios que los portales de los Sitios Enlazados ofrezcan.</p>
          <p>PROSEGUR no conoce los contenidos y servicios de los Sitios Enlazados por lo que no se hace responsable de los daños producidos por la ilicitud, calidad, desactualización, indisponibilidad, error e inutilidad de los contenidos o servicios de los Sitios Enlazados.</p>
          <p><b>3. Fuerza mayor</b></p>
          <p>Sin perjuicio de lo anterior, PROSEGUR no será responsable de los retrasos o fallos que se produjeran en el acceso, funcionamiento y operatividad del Sitio Web, sus Contenidos y/o Servicios, así como tampoco de las interrupciones, suspensiones o el mal funcionamiento del mismo, cuando tuvieren su origen en averías producidas por catástrofes naturales como terremotos, inundaciones, rayos o incendios, situaciones de fuerza mayor, situaciones de urgencia extrema tales como guerras, operaciones militares, disturbios civiles, huelgas, cierres patronales o cualquier otra situación de fuerza mayor o caso fortuito.</p>
          <p><b>4. Indemnización</b></p>
          <p>Los Usuarios se comprometen expresamente a indemnizar a PROSEGUR, a las sociedades de su grupo, empleados, administradores, agentes, y demás afectados, de los daños y perjuicios (incluidos honorarios de letrados, derechos de procurador y costas) derivados del incumplimiento por los Usuarios de estas condiciones generales y de las condiciones particulares aplicables en su caso, así como a colaborar con PROSEGUR en la defensa de sus intereses en el caso de que se presente cualquier reclamación o se inicie cualquier procedimiento judicial o administrativo por dicho incumplimiento. Asimismo, los Usuarios se obligan a realizar sus mayores esfuerzos a fin de evitar o, en su caso, minorar los efectos dañosos y perjudiciales que de ello pudieran derivarse para PROSEGUR.</p>
          <p>PROSEGUR no será responsable de (i) los daños indirectos y consecuenciales o de (ii) las pérdidas empresariales (incluyendo lucro cesante, de ingresos, de contratos, de ahorros previstos, de datos, pérdida del fondo de comercio o gastos innecesarios incurridos) que sean resultado directo o indirecto del acceso al Sitio Web por parte de los Usuarios. </p>
          <p><b>5. Advertencia de seguridad</b></p>
          <p>El “phising” y otras actuaciones fraudulentas de robo y suplantación de identidad de una persona, consisten en que terceras personas simulen identidad de una entidad mediante el uso fraudulento del correo electrónico para conseguir mediante engaño sus datos personales.</p>
          <p>En las relaciones que el Usuario mantenga o pretenda mantener con PROSEGUR debe tener en cuenta las siguientes recomendaciones:</p>
          <ul>
            <li>En todo caso, desde PROSEGUR nunca se solicitará al Usuario, ni por teléfono ni por correo electrónico, sus credenciales de acceso al Sitio Web. En este sentido, se recuerda al Usuario que estos datos son estrictamente confidenciales, y que sólo deben ser utilizados para el/los uso/usos a que se destinan. La cesión por parte del Usuario de sus datos confidenciales a terceras personas es de su exclusiva responsabilidad.</li>
            <li>PROSEGUR tampoco solicitará al Usuario por teléfono o por correo electrónico, copia de su documento nacional de identidad (DNI, NIE, pasaporte), ni dato alguno relativo a sus cuentas o posiciones bancarias. El Usuario debe desconfiar por tanto, en caso de no conocer de antemano y directamente a la persona que le solicite este tipo de datos en nombre o por cuenta de PROSEGUR.</li>
            <li>El Usuario no debe acceder al Sitio Web/App por medio de un enlace, e-mail o página web que no sea de su absoluta confianza. En su lugar, es recomendable que teclee directamente la dirección en el navegador.</li>
          </ul>
          <p><b>6. Seguridad</b></p>
          <p>El Usuario es responsable de la custodia, uso diligente y mantenimiento de la confidencialidad de las contraseñas, claves de acceso o sistemas de cifrado o encriptación de los equipos adquiridos y/o instalados.</p>
          <p>El Usuario acepta asumir la responsabilidad que proceda por todas las actividades realizadas desde su cuenta y/o utilizando su contraseña.</p>
          <p><b>7. Nulidad Parcial</b></p>
          <p>Si alguna cláusula del Aviso Legal fuera declarada inválida, nula o por cualquier causa ineficaz, dicha condición se entenderá excluida, sin que la validez ni la exigibilidad del resto de condiciones se vean afectadas por ello. </p>
          <p><b>8. Renuncia</b></p>
          <p>En caso de incumplimiento por parte de los Usuarios de las presentes Condiciones sin que PROSEGUR ejercitase inmediatamente las acciones y/o derechos que tuviese a su alcance, no implica que PROSEGUR renuncie a hacer uso de los mismos en cualquier otro momento en el futuro. </p>
          <p><b>9. Competencia y Jurisdicción</b></p>
          <p>El Aviso Legal de este Sitio Web se regirá por la legislación Española.</p>
          <p>PROSEGUR y los Usuarios, renuncian expresamente al fuero propio que pudiera corresponderles, y se someten formalmente a los Juzgados y Tribunales de Madrid, para cualquier controversia que pudiera surgir con motivo de la interpretación o el cumplimiento del Aviso Legal por el que se rige este Sitio Web.</p>

         </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="p-condiciones" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Aviso Legal</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Cuota de servicio hogar kit básico+ Pack Visión Total 1,32 € al día hogar/ 1,45€ negocio. IVA no incluido. DGP 4124. Período mínimo de permanencia de 2 años.</p>
          <p>El pack Visión Total incluye una cámara, para beneficiarse de esta oferta es necesario contratar alarma y cámara conjuntamente.</p>
          <p>Alta Pack Visión Total no incluida</p>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  <section class="section-1">
    <div class="container-fluid">

      <!-- Landing Title -->
      <div class="position-absolute title-wrapper d-flex justify-content-center">
        <h1 class="container title">
          <?php if ($title == 0): ?>
            <span>La tranquilidad de disponer del</span><br>
            <span><strong> mejor sistema de alarma</strong></span><br>
            <span>en tu hogar</span>
          <?php else: ?>
            <span><strong>Servicio Siempre Contigo</strong></span><br>
            <span>para mayores y familiares</span>
          <?php endif; ?>
        </h1>
      </div>

      <!-- IMG + Form -->
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-image bg <?php echo $background; ?>"></div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-form d-flex align-items-center justify-content-start">
          <div class="position-relative form-wrapper" id="formContainer">
            <img src="./assets/img/logo-para-web.jpg" alt="logo-para-web-form">
            <?php if ($title == 1): ?>
              <img src="./assets/img/01_euro.png" alt="un-euro" class="position-absolute euro">
            <?php else: ?>
              <img src="./assets/img/badge_descuento.png" class="position-absolute descuento">
            <?php endif; ?>
            <form id="callForm">
              <div class="form-group">
                <label for="phoneNumber">Teléfono</label>
                <input type="number" class="form-control" id="phoneNumber" placeholder="Por ejemplo: 600000000">
              </div>
              <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="privacyCheck">
                <label class="form-check-label" for="privacyCheck">He leido y acepto la <a href="#" data-toggle="modal" data-target="#p-privacidad">política de privacidad</a></label>
              </div>
              <div class="error-group">
                <div class="text-danger error-message d-none" id="phoneError">El número de teléfono no es correcto.</div>
                <div class="text-danger error-message d-none" id="privacyError">Debes aceptar la política de privacidad.</div>
                <div class="text-danger error-message d-none" id="unknownError">Tenemos problemas para procesar tu solicitud, inténtalo de nuevo en unos minutos.</div>
              </div>
              <button type="submit" id="submitButton" class="btn btn-primary">
                <img src="assets/img/spinner.png">
                <span>TE LLAMAMOS</span>
              </button>
              <!--<p class="small mt-3 text-center">Oferta válida hasta el 30/06/2018.</p>-->
              <!--<p class="d-block d-lg-none condiciones" data-toggle="collapse" data-target="#narrow-results">Ver condiciones</p>-->
              <!--<p class="small collapse" id="narrow-results">Cuota de servicio hogar kit básico+ Pack Visión Total 1,32 € al día hogar/ 1,45€ negocio. IVA no incluido. DGP 4124. Período mínimo de permanencia de 2 años.<br>El pack Visión Total incluye una cámara, para beneficiarse de esta oferta es necesario contratar alarma y cámara conjuntamente.<br>Alta Pack Visión Total no incluida. Para el resto de condiciones llama al 902 202 999.</p>-->
            </form>
            <div class="form-footer">
              <img src="./assets/img/por_menos.png" alt="por-menos" class="por-menos">
            </div>
          </div>
          <div class="col-12 col-xl-6 p-0 d-none" id="thankYou">
            <div class="content d-flex align-items-center">
              <div class="info">
                <h2>Gracias por contactar con <strong>PROSEGUR</strong></h2>
                <p>Pronto le llamaremos al teléfono que nos ha facilitado<p>
                  <div id="conversionPixel"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php if ($title == 0): ?>
      <section class="cta">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12 center">
              <h2>Solo este mes <strong>¡200€ de descuento!</strong></h2>
            </div>
          </div>

        </div>
      </section>
    <?php endif; ?>
    <section class="section-2">      
      <div class="container">
        <div class="row">
          <?php if ($title == 1): ?>
            <div class="col-12 center">
              <h3>¿Cómo funciona y que ofrece PROSEGUR SIEMPRE CONTIGO?</h3>
            </div>
          <?php endif; ?>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-image d-flex justify-content-center align-items-center">
            <?php if ($title == 1): ?>
              <img src="./assets/img/siempre-contigo.png" alt="triple-seguridad">
            <?php else: ?>
              <!--<img src="./assets/img/grafico-proceso-triple-seguridad.png" alt="triple-seguridad">-->
            <?php endif; ?>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-text">

            <?php if ($title == 1): ?>
              <p class="small-title m-0">Aviso de caídas</p>
              <p>
                Ante una caída, nuestro Centro de Atención recibe una señal de emergencia, avisando a un familiar y a los servicios de emergencia si fuera necesario
              </p>
              <p class="small-title m-0">Botón Emergencias</p>
              <p>
                Basta con pulsar un botón y el Centro de Atención contactará y actuará según la emergencia
              </p>
              <p class="small-title m-0">Aviso de inactividad</p>
              <p>
                Aviso al familiar siempre que el dispositivo permanezca inactivo más de 48 horas.
              </p>
              <p class="small-title m-0">Llamadas de seguimiento</p>
              <p>
                Cada 15 días haremos una llmada de seguimiento desde nuestro Centro de Atención de Prosegur. Además, informaremos al familiar seleccionado del resultado de la llamada.
              </p>
              <p class="small-title m-0">Llamadas gratuitas</p>
              <p>
                4 botones de marcación rápida para contactar con el Centro de Atención o la familia convierten al dispositivo en un móvil muy sencillo de utilizar.
              </p>
              <p class="small-title m-0">Plataforma personalizada</p>
              <p>
                Web para configurar múltiples alertas sobre:
              </p>
              <p class="small-title-2 m-0">Geolocalización</p>
              <p>
                Plataforma personalizada. Localización constante del dispositivo para evitar casos de desorientación o pérdida.
              </p>
              <p class="small-title-2 m-0">Zona Segura</p>
              <p>
                El familiar seleccionado recibirá un aviso siempre que el dispositivo salga de la zona establecida.
              </p>
              <p class="small-title-2 m-0">Seguimiento</p>
              <p>
                Permite visualizar el recorrido realizado.
              </p>
              <p class="small-title-2">Otros servicios disponibles: videovigilancia, custodia de llaves.</p>
            <?php else: ?>
              <!--<h3>¿Cómo funciona la Triple Seguridad?</h3>
              <p class="small-title">Seguridad ante intrusión</p>
              <p>
                Nuestras alarmas son antirrobo. Si se produce una intrusión, los videodetectores
                instalados en la vivienda se activan y graban imágenes de lo que está pasando
                en tu casa, enviándolas a la Central Receptora de Alarmas. Si se verifica el salto de
                alarma y en caso de ser necesario, Prosegur avisará a laPolicia de manera inmediata.
              </p>
              <p class="small-title">Seguridad ante inhibición</p>
              <p>
                Los equipos de Prosegur son a prueba de inhibidores. Disponen de doble vía de comunicación,
                si se inhibe la señal GPSRS de la central, la alarma seguirá funcionando utilizando la línea
                de respaldo IP. El sistema no queda anulado en ningún momento, y si se produceun intento de intrusión,
                sigue enviando la señal a la Central Receptora de Alarmas para comprobar si la intrusión es real o no y avisar a
                la Policía.
              </p>
              <p class="small-title">Seguridad ante sabotaje</p>
              <p>
                La última innovación de nuestros sistemas es el SAI, una batería auxiliar incorporada en el router
                (a través del que se transmite la comunicación IP) que permite que la alarma mantenga la doble vía
                de comunicación en caso de corte eléctrico. El sistema seguirá conectado a nuestra CRA.
              </p>-->
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>

    <section class="section-3 pt-0">      
      <?php if ($title == 0): ?>
        <!--<div class="container-fluid">
          <div class="row">
            <div class="col-12 col-lg-4 position-relative border" style="background-image:url('assets/img/background-hogar.png');background-size: cover; background-position: center; background-repeat: no-repeat">
              <div class="info">
                <h3>Triple seguridad <strong>para tu casa</strong></h3>
                <p>Para proteger a los que más quieres, sólo puedes confiar en la alarma del líder en seguridad. Si quieres vivir tranquilo, elige Prosegur.</p>
                <p>La alarma Prosegur es un sistema con verificación a través de imagen y aviso a Policía.</p>
                <p>Ante un salto de alarma, los detectores envían una señal a la Central Receptora de Alarmas y nos ponemos en contacto con el cliente rápidamente para comprobar su veracidad.</p>
                <p>Si es un salto de alarma real, avisamos inmediatamente a la Policía.</p>
              </div>
            </div>
            <div class="col-12 col-lg-4 position-relative border" style="background-image:url('assets/img/background-negocio.png');background-size: cover; background-position: center; background-repeat: no-repeat">
              <div class="info">
                <h3>Triple seguridad <strong>para tu negocio</strong></h3>
                <p>Alarma para negocio Prosegur. Disfruta de la protección de tu alarma Prosegur y vive tranquilo. El sistema permite una conexión constante con la Central Receptora de Alarmas de Prosegur, y cuenta con doble vía de comunicación, ya que además de GPRS, transmite a través de la línea IP. Prosegur ha desarrollado la mejor alternativa para que sólo te preocupes en hacer crecer tus beneficios. Te ofrecemos una solución adaptada a tus necesidades.</p>
              </div>
            </div>
            <div class="col-12 col-lg-4 position-relative border" style="background-image:url('assets/img/background-videovigilancia.png');background-size: cover; background-position: center; background-repeat: no-repeat">
              <div class="info">
                <h3><span class="invisible">Triple seguridad</span><strong>Videovigilancia</strong></h3>
                <p>Si quieres ver lo que pasa en tu hogar o negocio desde cualquier lugar elige la videovigilancia. Es un sistema equipado con la última tecnología que consigue grabaciones de alta calidad. Con la videovigilancia, tu hogar o negocio estará conectado las 24 horas, los 365 días del año a nuestra Central Receptora de Alarmas. Además, disponemos de un tipo de cámara para cada cliente, analógicas o cámaras IP.</p>
              </div>

            </div>
          </div>
        </div>-->
        <div class="promo-integral">  
          <div class="macheta">
            <p>
              <span>Descubre ya la <strong>protección integral</strong></span>
            </p>
          </div>        
          <div class="container-fluid">
            <div class="row">
              <div class="col-12 position-relative">                
                <h2>                  
                  Tu alarma <strong>PROSEGUR</strong> ahora incluye
                </h2>
              </div>
            </div>
            <div class="row justify-content-center">
              <div class="col-12 col-md-6 col-lg-3 item">
                <div class="image" style="background-image: url('assets/img/house.jpeg')">
                </div>
                <h3>Robos</h3>
                <p>Las alarmas Prosegur te ofrecen <b>protección total</b> ante cualquier ataque o intrusión. Están conectadas 24 horas, 365 días al año, a una Central Receptora de Alarmas, cuyos profesionales, en casa necesario, <b>avisan de manera inmediata a la Policía.</b></p>
              </div>
              <div class="col-12 col-md-6 col-lg-3 item">
                <div class="image" style="background-image: url('assets/img/detector-humos.jpeg')">
                </div>
                <h3>Incendios</h3>
                <p>Prosegur suma a su sistema de seguridad un <b>detector de humo</b> que previene los daños ante posibles incendios. En caso de detección, envía un aseñal a la Central Receptora de Alarmas, desde allí, siempre que sea necesario, <b>Avisarán a los bomberos</b>.
              </div>
              <div class="col-12 col-md-6 col-lg-3 item">
                <div class="image" style="background-image: url('assets/img/control-de-imagen.jpg')">
                </div>
                <h3>Control</h3>
                <p>Saber que tu hogar y lo que más te importa está protegido, es una prioridad. Además, si a esto le sumas, la tranquilidad de saber lo que sucede en tu casa en todo momento, se convierte en una protección total. <b>Disfruta del control de saber todo l oque sucede en tu casa </b>.
              </div>
              <!--<div class="col-12 col-md-6 col-lg-3 item">
                <div class="image" style="background-image: url('assets/img/detector-humos.jpeg')">                  
                </div>
                <h3>Detector de humo gratis</h3>
              </div>-->
            </div>
          </div>
        </div>
        <!--<div class="promo-integral" style="background-image: urL('assets/img/tranquilidad.jpeg')">
          <div class="container">
            <div class="row">
              <div class="col-12">
                <h2>Tu alarma <strong>PROSEGUR</strong> ahora incluye</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-12 col-md-8">
                <div class="item">
                  <h3><img src="assets/img/png/001-web-page-home.png"/> Alarma PROSEGUR</h3>
              
                  <h3><img src="assets/img/png/002-open-laptop-computer.png"/> Protección ante robos, ante incendio y ante ciberdelincuencia</h3>
                
                  <h3><img src="assets/img/png/003-security.png"/> Un año de antivirus gratis</h3>
                
                  <h3><img src="assets/img/png/004-no-fire.png"/> Detector de humo gratis</h3>
                </div>
              </div>
            </div>
          </div>
        </div>-->
        <!--<div class="container mt-5">
          <div class="row">
            <div class="col-12">
             <h2 class="text-center">Contrata ahora tu pack <strong>VISIÓN TOTAL.</strong><br>Ahora con 3 cámaras analógicas</h2>
            </div>
           </div>
           <div class="row">
            <div class="col-12 col-md-6">
              <h3>Descripción del kit</h3>
              <ul>
                <li><strong>3 cámaras analógicas + videograbador</strong></li>
                <li>Central</li>
                <li>Teclado con sirena incorporada</li>
                <li>1 Detector sin cámara</li>
                <li>2 Detectores con cámara</li>
                <li>1 SAI</li>
                <li>1 Mando</li>
                <li>2 Tags</li>
              </ul>
            </div>
            <div class="col-12 col-md-6">
              <h3>Supervisión</h3>
              <ul>
                <li>Aviso a la Policía</li>
                <li>Conexión a CRA 24 horas</li>
                <li>Aviso de corte eléctrico</li>
                <li>Monitorización de comunicaciones</li>
                <li>Aviso ante cancelación de alarma</li>
                <li>Contraclave</li>
              </ul>
            </div>
           </div>
        </div>-->
        <!--<div class="container-fluid mt-5">
          <div class="row">
            <div class="col-12 col-md-6 vision-item p-3" style="background-image:url('assets/img/vision-01.png');background-size: cover; background-position: center; background-repeat: no-repeat">
              <div class="info text-center">
                <h3>pack VISIÓN TOTAL <strong>HOGAR</strong></h3>
                <p>Central + Teclado + Sirena interior cable</p>
                <p>1 Detector sin cámara + 2 Detectores con cámara</p>
                <p>1 SAI + 1 Mando + 2 tags</p>
              </div>
            </div>
            <div class="col-12 col-md-6 vision-item  p-3" style="background-image:url('assets/img/vision-02.png');background-size: cover; background-position: center; background-repeat: no-repeat">
              <div class="info text-center">
                <h3>pack VISIÓN TOTAL <strong>NEGOCIO</strong></h3>
                <p>Central + Teclado con sirena incorporada</p>
                <p>1 Detector sin cámara + 2 Detectores con cámara</p>
                <p>1 SAI + 1 Mando + 2 tags</p>
              </div>
            </div>
          </div>
        </div>-->
      <?php endif; ?>
      <?php if ($title == 0): ?>
        <div class="container-fluid smart">
          <div class="row">
            
            <div class="col-12 col-lg-6 d-flex justify-content-center align-items-center text-center">
              <div class="text-content">
                <h2><strong>Prosegur Smart</strong></h2>
                <h3>Tu sistema de alarma en la palma de la mano</h3>
                <p class="mb-4">Controla en todo momento tu sistema de alarma desde tu Smartphone o Tablet</p>
                <div class="row">
                  <div class="col-12 col-md-4">
                    <img src="assets/img/002-siren.png"/>
                    <span class="d-block mt-3">Conecta y desconecta tu alarma</span>
                  </div>
                  <div class="col-12 col-md-4">
                    <img src="assets/img/001-surveillance-camera.png"/>
                    <span class="d-block mt-3">Controla lo que sucede en tu hogar o negocio estés donde estés</span>
                  </div>
                  <div class="col-12 col-md-4">
                    <img src="assets/img/003-smartphone-call.png"/>
                    <span class="d-block mt-3">Recibe imágenes en alta calidad en tiempo real</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 p-0" style="background-image: url('assets/img/smart.jpeg'); background-size: cover; background-repeat: no-repeat;">
              <div class="badges-apps">
                <a href='https://play.google.com/store/apps/details?id=com.prosegur.smart20&hl=es&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
                  <img src="assets/img/google-play-badge.png"/>
                </a>
                <a href="https://itunes.apple.com/es/app/prosegur-smart-2-0/id1056557432?mt=8" target="_blank">
                  <img src="assets/img/apple-store.svg"/>
                </a>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div class="container mt-5">
        <div class="row">
          <div class="center">
            <h2><strong>519.506 personas</strong> confían en nosotros</h2>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-form">
            <div class="position-relative form-wrapper" id="formContainer-2">
              <img src="./assets/img/logo-para-web.png" alt="logo-para-web-form">
              <?php if ($title == 1): ?>
                <img src="./assets/img/01_euro.png" alt="un-euro" class="position-absolute euro">
              <?php endif; ?>
              <form id="callForm-2">
                <div class="form-group">
                  <label for="phoneNumber-2">Teléfono</label>
                  <input type="number" class="form-control" id="phoneNumber-2" placeholder="Por ejemplo: 600000000">
                </div>
                <div class="form-group form-check">
                  <input type="checkbox" class="form-check-input" id="privacyCheck-2">
                  <label class="form-check-label" for="privacyCheck-2">He leido y acepto la <a href="#" data-toggle="modal" data-target="#p-privacidad">política de privacidad</a></label>
                </div>
                <div class="error-group">
                  <div class="text-danger error-message d-none" id="phoneError-2">El número de teléfono no es correcto.</div>
                  <div class="text-danger error-message d-none" id="privacyError-2">Debes aceptar la política de privacidad.</div>
                  <div class="text-danger error-message d-none" id="unknownError-2">Tenemos problemas para procesar tu solicitud, inténtalo de nuevo en unos minutos.</div>
                </div>
                <button type="submit" id="submitButton-2" class="btn btn-primary">
                  <img src="assets/img/spinner.png">
                  <span>TE LLAMAMOS</span>
                </button>
                <!--<p class="small mt-3 text-center">Oferta válida hasta el 30/06/2018.</p>-->
                <!--<p class="d-block d-lg-none condiciones" data-toggle="collapse" data-target="#narrow-results">Ver condiciones</p>-->
                <!--<p class="small collapse" id="narrow-results-2">Cuota de servicio hogar kit básico+ Pack Visión Total 1,32 € al día hogar/ 1,45€ negocio. IVA no incluido. DGP 4124. Período mínimo de permanencia de 2 años.<br>El pack Visión Total incluye una cámara, para beneficiarse de esta oferta es necesario contratar alarma y cámara conjuntamente.<br>Alta Pack Visión Total no incluida. Para el resto de condiciones llama al 902 202 999.</p>-->
              </form>
            </div>
            <div class="col-12 col-xl-6 p-0 d-none" id="thankYou-2">
              <div class="content d-flex align-items-center">
                <div class="info">
                  <h2>Gracias por contactar con <strong>PROSEGUR</strong></h2>
                  <p>Pronto le llamaremos al teléfono que nos ha facilitado<p>
                    <div id="conversionPixel-2"></div>
                  </div>
                </div>
              </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-info">
            <table>
              <thead>
                <tr>
                  <td class="big empty"></td>
                  <td class="small">
                    <img src="./assets/img/logo-para-web.jpg" alt="logo-web" class="logo-table">
                  </td>
                  <td class="small grey">Otros servicios de alarma</td>
                </tr>
              </thead>
              <tbody>
                <!--<tr>
                  <td class="big empty">
                    Actualizamos tu alarma de forma gratuita
                  </td>
                  <td class="small">
                    <i class="fas fa-check-circle"></i>
                  </td>
                  <td class="small">
                    <i class="fas fa-times-circle"></i>
                  </td>
                </tr>-->

                <tr>
                  <td class="big empty">
                    <?php if ($title == 1): ?>
                      En menos de 30 segundos, enviamos ayuda.
                    <?php else: ?>
                      En menos de 30 segundos, avisamos a la policía
                    <?php endif; ?>

                  </td>
                  <td class="small">
                    <i class="fas fa-check-circle"></i>
                  </td>
                  <td class="small">
                    <i class="fas fa-times-circle"></i>
                  </td>
                </tr>
                <tr>
                  <td class="big empty">
                    Un experto en seguridad acreditado te asesorará
                  </td>
                  <td class="small">
                    <i class="fas fa-check-circle"></i>
                  </td>
                  <td class="small">
                    <i class="fas fa-times-circle"></i>
                  </td>
                </tr>
                <tr>
                  <td class="big empty">
                    <?php if ($title == 1): ?>
                      Servicio Siempre Contigo, para mayores y sus familias
                    <?php else: ?>
                      Controla la seguridad de tu hogar desde el móvil
                    <?php endif; ?>
                  </td>
                  <td class="small">
                    <i class="fas fa-check-circle"></i>
                  </td>
                  <td class="small">
                    <i class="fas fa-times-circle"></i>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>


    <footer>

      <div class="cookies-banner d-none">
        <h3>Uso de Cookies</h3>
        <p>PROSEGUR utiliza Cookies propias para elaborar estadísticas y
          mostrar productos relacionados con sus preferencias mediante el
          análisis de sus hábitos de navegación. Si continua navegando o
          pulsa sobre el botón aceptar consideramos que acepta su uso.
          <a href="#" class="d-inline-block" data-toggle="modal" data-target="#p-cookies">¿Qué son las cookies?</a>
        </p>
        <div class="bnt-cookies-accept">Aceptar</div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-12 col-xl-12 p-0 d-flex justify-content-center">
            <ul>
              <li><a href="#" data-toggle="modal" data-target="#p-legal" class="d-inline-block">Aviso legal</a></li>
              <li><a href="#" data-toggle="modal" data-target="#p-privacidad" class="d-inline-block">Política de privacidad</a></li>
              <li><a href="#" class="d-inline-block" data-toggle="modal" data-target="#p-cookies">Política de cookies</a></li>
              <!-- <li><a href="#" class="d-inline-block" data-toggle="modal" data-target="#p-condiciones">(*) Condiciones de la promoción</a></li> -->
            </ul>
          </div>
        </div>
      </div>

      <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
      <script src="assets/js/env.js"></script>
      <script src="assets/js/app.js"></script>
    </footer>
  </body>
  </html>
