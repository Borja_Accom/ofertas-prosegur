function showForm(form = ''){
    $("#thankYou"+form).addClass("d-none");
    $("#formContainer"+form).removeClass("d-none");
    $("#conversionPixel"+form).html("");
}

function showThankYou(transactionId, form = '', come_from_email){
    $("#formContainer"+form).addClass("d-none");
    $("#thankYou"+form).removeClass("d-none");

    var provider = +$('#provider').val();
    //El provider tiene que ser entre [1..13]
    var provider_ok = (provider > 0 && provider < 14) ? true : false;

    if (come_from_email && provider_ok) {
      var pixel = getPixel(provider, transactionId);
      $("<img></img>").attr("src", pixel)
      .attr("width", "1")
      .attr("height", "1")
      .attr("border", "0")
      .appendTo("#conversionPixel"+form);
    }

}

/**
 * Retorna el pixel dependiendo del provider
 * @param  {int} privider      valor del parámetro provider
 * @param  {int} transactionId id que devuelve el webservice al devolver OK
 * @return {string}            src del pixel dependiendo del provider
 */
function getPixel(provider, transactionId) {
  switch (provider) {
    case 1:
        pixel_src = 'https://track.globalldse.com/aff_l?offer_id=6806&adv_sub='+transactionId;
        break;
    case 2:
        pixel_src = 'https://lot.neatpowr.com/trk_o?px=gDCZntNB1XA%3d&adv_sub='+transactionId;
        break;
    case 3:
        pixel_src = 'https://www.afilead.com/afiliacion/scripts/sale.php?AccountId=0a45e01f&TotalCost=0&OrderID='+transactionId+'&ProductID=0&CampaignID=2566b5ea';
        break;
    case 4:
        pixel_src = 'https://www.dbevt.com/35b535c1-e3c3-4462-88ed-7c401fe046e9/2.gif';
        break;
    case 5:
        pixel_src = 'https://track.globalldse.com/aff_l?offer_id=6856&adv_sub='+transactionId;
        break;
    case 6:
        //pixel_src = 'https://track.globalldse.com/aff_l?offer_id=6856&adv_sub='+transactionId;
        break;
    case 7:
        pixel_src = 'https://t.viprsp.nl/global_postback/'+transactionId;
        break;
    case 8:
        pixel_src = 'https://trk.contentignition.net/?r=px/c2s&key=ESC1GM2BH7RF5PK0A';
        break;
    case 9:
        pixel_src = '';
        break;
    case 10:
        pixel_src = '';
        break;
    case 11:
        pixel_src = 'https://www.smart4ads.com/smart4ads/api/PVT.php?accountid=fd9c0421&totalcost=0&orderid='+transactionId+'&actioncode=ProsegurACCOMCPL';
        break;
    case 12:
        pixel_src = 'http://affiliation.datawork.fr/aff_l?offer_id=996';
        break;
    case 13:
        pixel_src = 'http://tracking.feebbo-adserver.com/aff_l?offer_id=7662';
        break;
  }
  return pixel_src;
}

function startLoading(form = ''){
    $("#unknownError"+form).addClass("d-none");
    $("#submitButton"+form).addClass("loading").attr("disabled", "disabled");
}

function stopLoading(form = ''){
    $("#submitButton"+form).removeClass("loading").removeAttr("disabled");
}


$(function(){

    $(document).on("keyup", "#phoneNumber, #phoneNumber-2", function(){
        var phone = $(this).val().replace(" ", "");
        $(this).val(phone);
    });

    $(document).on("submit", "#callForm, #callForm-2", function(e){
        var form_id = ($(this).attr('id') == 'callForm-2') ? '-2' : '';
        e.preventDefault();
        var error = false,
            phone = $("#phoneNumber"+form_id).val();

        if(!phone.match(/^([6789]{1})([0-9]{8})$/)){
            error = true;
            $("#phoneError"+form_id).removeClass("d-none");
        }else{
            $("#phoneError"+form_id).addClass("d-none");
        }

        if(!$("#privacyCheck"+form_id).is(":checked")){
            error = true;
            $("#privacyError"+form_id).removeClass("d-none");
        }else{
            $("#privacyError"+form_id).addClass("d-none");
        }

        if(error == false){
            startLoading(form_id);
            var id_landing = $('#id_landing').val();
            var platform = $('#platform').val();
            var ws_url = WEBSERVICE_URL + "?telefono=" + phone + "&platform=" + platform + "&creativity=" + id_landing;

            if ($('#soporte').length) {
              ws_url = ws_url+'&soporte='+$('#soporte').val();
            }
            if ($('#date').length) {
              ws_url = ws_url+'&date='+$('#date').val();
            }
            if ($('#id_origen').length) {
              ws_url = ws_url+'&id_origen='+$('#id_origen').val();
            }

            $.ajax({
                url:  ws_url,
                type: 'GET',
                contentType: 'text/plain',
                xhrFields: {
                    withCredentials: false
                },
                beforeSend: function( xhr ) {
                    xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
                }
            })
            .done(function(response) {
                stopLoading(form_id);
                try {
                    var data = JSON.parse(response);
                    var come_from_email = (platform == 'email') ? true : false;
                    showThankYou(data['ResponseIds']['IdTransaccion'], form_id, come_from_email);
                }
                catch(err) {
                    $("#unknownError"+form_id).removeClass("d-none");
                }
            })
            .fail(function(data){
                stopLoading(form_id);
                $("#unknownError"+form_id).removeClass("d-none");
            });
        }
    });

})
