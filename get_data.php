<?php
    if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest')
    {
        http_response_code(403);
        die('Forbidden');
    }



    function curl_get($url, array $get = NULL, array $options = array())
    {
        $defaults = array(
            CURLOPT_URL => $url. (strpos($url, '?') === FALSE ? '?' : ''). http_build_query($get),
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 4
        );
        $ch = curl_init();
        curl_setopt_array($ch, ($options + $defaults));
        if( ! $result = curl_exec($ch))
        {
            trigger_error(curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }


    function curl_post($url, array $fields = NULL) {
      //open connection
      $ch = curl_init();

      //set the url, number of POST vars, POST data
      curl_setopt($ch,CURLOPT_URL, $url);
      curl_setopt($ch,CURLOPT_POST, count($fields));
      curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($fields));

      //execute post
      if( ! $result = curl_exec($ch))
      {
          trigger_error(curl_error($ch));
      }

      //close connection
      curl_close($ch);
      return $result;
    }

    if(isset($_GET['telefono'])){
        $fields =  [
          "Telefono" => $_GET['telefono'],
          "Canal" => $_GET['platform'],
          "Creatividad" => $_GET['creativity']
        ];

        if (isset($_GET['soporte'])) {
          $fields["Soporte"] = $_GET['soporte'];
        }
        if (isset($_GET['date'])) {
          $fields["Envio"] = $_GET['date'];
        }
        if (isset($_GET['id_origen'])) {
          $fields["IDOrigen"] = $_GET['id_origen'];
        }

        $call = curl_post("http://163.172.226.207/WebService/v2/accom/generico/?format=json&Estrategia=CallMe_Prosegur_C2C", $fields);
        //$call = curl_post("http://163.172.226.207/WebService/v2/accom/generico/?format=json&Estrategia=CallMe_Prueba", $fields);

        $response = json_decode($call, true);
        if($response){
            http_response_code(200);
        }else{
            http_response_code(400);
        }
        //echo $call;
    }else{
        http_response_code(400);
        echo "Parámetro teléfono requerido";
    }

?>
